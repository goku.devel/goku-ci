<?php

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/**
 * Include the Pantheon-specific settings file.
 *
 * n.b. The settings.pantheon.php file makes some changes
 *      that affect all environments that this site
 *      exists in.  Always include this file, even in
 *      a local development environment, to ensure that
 *      the site settings remain consistent.
 */
include __DIR__ . "/settings.pantheon.php";

/**
 * Place the config directory outside of the Drupal root.
 */
$settings['config_sync_directory'] = dirname(DRUPAL_ROOT) . '/config/sync';

/**
 * Setup environments settings.
 */
if (isset($_SERVER['PANTHEON_ENVIRONMENT']) && php_sapi_name() != 'cli') {
  // Pantheon Dev settings
  if ($_SERVER['PANTHEON_ENVIRONMENT'] === 'dev') {
    // Config split
    $config['config_split.config_split.dev']['status'] = TRUE;
  }
  // Pantheon Test settings
  if ($_SERVER['PANTHEON_ENVIRONMENT'] === 'test') {
    // Config split
    $config['config_split.config_split.test']['status'] = TRUE;
  }
  // Pantheon Live settings
  if ($_ENV['PANTHEON_ENVIRONMENT'] === 'live'):
    /** Replace www.example.com with your registered domain name */
    // $primary_domain = 'www.example.com';
    // Config split
    $config['config_split.config_split.live']['status'] = TRUE;
  else:
    // Redirect to HTTPS on every Pantheon environment.
    // $primary_domain = $_SERVER['HTTP_HOST'];
  endif;
  // $base_url = 'https://'. $primary_domain;
  // if ($_SERVER['HTTP_HOST'] != $primary_domain
  //     || !isset($_SERVER['HTTP_X_SSL'])
  //     || $_SERVER['HTTP_X_SSL'] != 'ON' ) {
  //   header('HTTP/1.0 301 Moved Permanently');
  //   header('Location: '. $base_url . $_SERVER['REQUEST_URI']);
  //   exit();
  // }

} else {
  /**
   * If there is a local settings file, then include it
   */
  $local_settings = __DIR__ . "/settings.local.php";
  if (file_exists($local_settings)) {
    include $local_settings;
    //Config split
    $config['config_split.config_split.dev']['status'] = TRUE;
  }
}

$settings['file_temp_path'] = '/tmp/';